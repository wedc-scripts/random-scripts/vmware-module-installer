# USAGE

`sudo ./vmware-installer.sh vmware-edition-version.bundle`
`reboot`

---

## How it work

---

This script will get from the bundle filename of VMware downloaded by the user, the edition and version needed to download the VMware host modules for the kernel from the repository [mkubecek/vmware-host-modules](<[https://](https://github.com/mkubecek/vmware-host-modules)>).

if the user have secure boot enabled the user will need to restart the machine, then when the mok ask for the user to enroll the MOK the user select '**enroll MOK**' > **Continue** > **put root PASSWORD** and then continue with the process normally.

This then will proceed to create a folder in `/tmp/`, do the make and make install, when this finish will proceed to execute the bundle to finish the installation.

---

## Functions in the scripts

- Detect OS
- Install needed dependencies to install the module, if the user have secure boot enable will sign in the modules.
- Add function to suspend the VMs when the host machine go to sleep or hibernate.
- Create the needed systemd unit services for the correct function of VMware and fix VMWare init LSB
- Fix some permissions and group of VMware devices.
- the group `wheel` can use all VMware functions.
- Install the bundle
