#!/usr/bin/env bash
# -*- coding: utf-8 -*-

# ==============================================================================
# title           : VMWare Installer dependencies
# description     : This Script is intended to install dependencies, modules and setup the enviroment for a full functionality of VMWare Workstantion/Player instance
# author		  : Walddys Emmanuel Dorrejo Céspedes
# date            : 2022/08/02
# updated         : 2022/11/01
# usage		      : bash vmware-installer.sh vmware-edition-version.bundle
# notes           : Execute this Script like sudo with vmware bundle file
# ==============================================================================

function sudo_v() {
    if (($(id -u) != 0)); then
        clear
        echo "Execute the script with sudo"
        sleep 3s
        clear
        exit 15
    fi
}

function arguments_validate() {
    if [ $# -eq 0 ]; then
        echo "No arguments supplied"
        exit 1
    fi
}

function detect_distro() {
    if [ -f /etc/os-release ]; then
        # shellcheck source=/dev/null
        . /etc/os-release
        OS=$NAME
    elif type lsb_release >/dev/null 2>&1; then
        OS=$(lsb_release -si)
    elif [ -f /etc/lsb-release ]; then
        # shellcheck source=/dev/null
        . /etc/lsb-release
        OS="$DISTRIB_ID"
    elif [ -f /etc/debian_version ]; then
        OS=Debian
    else
        OS=$(uname -s)
    fi
}

function install_dependencies() {
    case $OS in
    openSUSe* | OpenSUSE* | openSUSE*)
        if rpm -qa | grep -qi kernel-devel && rpm -qa | grep -qi desktop-file-utils && rpm -qa | grep -qEi ^git && rpm -qa | grep -qi make; then
            echo "Dependencies are previously installed, not need to install it"
        else
            zypper in -y kernel-devel desktop-file-utils make
            zypper in -y --no-recommends git
        fi
        if grep -qi tumbleweed /etc/os-release &>/dev/null; then
            zypper in -y systemd-sysvcompat
        fi
        ;;

    Debian* | Ubuntu)
        echo "Not yet compatible, please contribute with the needed command and package name to be installed like dependency: https://gitlab.com/wedc-scripts/random-scripts/vmware-module-installer/-/issues"
        ;;

    CentOS*)
        echo "Not yet compatible, please contribute with the needed command and package name to be installed like dependency: https://gitlab.com/wedc-scripts/random-scripts/vmware-module-installer/-/issues"
        ;;

    esac
}

function file_permissions_vmnet_group() {
    if ! grep -q wheel /etc/group; then
        groupadd wheel
    fi

    chgrp wheel /dev/vmnet0
    chmod g+rw /dev/vmnet0
    usermod -aG wheel "${SUDO_USER}"

}

# function file_permissions_vmnet_all_user() {
#     chmod a+rw /dev/vmnet0
# }

function init.d-fix() {
    if [ ! -d "/etc/init.d" ]; then
        mkdir /etc/init.d
    fi
    for x in {0..6}; do
        if [ ! -d "/etc/init.d/rc$x.d" ]; then
            mkdir -p /etc/init.d/rc"$x".d
        fi
    done
}

function create_vmware.service() {
    SYSTEMD_EDITOR="tee" systemctl edit --full --force vmware.service <<EOF
[Unit]
Description=VMware daemon
Requires=vmware-usbarbitrator.service
Before=vmware-usbarbitrator.service
After=network.target

[Service]
ExecStart=/etc/init.d/vmware start
ExecStop=/etc/init.d/vmware stop
PIDFile=/var/lock/subsys/vmware
RemainAfterExit=yes

[Install]
WantedBy=multi-user.target
EOF

    SYSTEMD_EDITOR="tee" systemctl edit --full --force vmware-usbarbitrator.service <<EOF
[Unit]
Description=VMware USB Arbitrator
Requires=vmware.service
After=vmware.service

[Service]
ExecStart=/usr/bin/vmware-usbarbitrator
ExecStop=/usr/bin/vmware-usbarbitrator --kill
RemainAfterExit=yes

[Install]
WantedBy=multi-user.target
EOF

    SYSTEMD_EDITOR="tee" systemctl edit --full --force vmware-networks-server.service <<EOF

[Unit]
Description=VMware Networks
Wants=vmware-networks-configuration.service
After=vmware-networks-configuration.service

[Service]
Type=forking
ExecStartPre=-/sbin/modprobe vmnet
ExecStart=/usr/bin/vmware-networks --start
ExecStop=/usr/bin/vmware-networks --stop

[Install]
WantedBy=multi-user.target
EOF

    systemctl daemon-reload
    systemctl enable --now vmware-networks-server.service vmware-usbarbitrator.service vmware.service
}

function module_compilation {
    set -x
    mkdir -p /tmp/vmware && cd "$_" || echo "Permission denied: Can't create folder"
    WORKDIR="$(pwd)"
    EDITION=$(basename "$1" | grep -iPo "(?<=vmware.)(.*)(?=.full)")
    VERSION=$(basename "$1" | grep -iPo "(?<=-)\d+(\.\d+)+(.*?)(?=\-)")
    echo "Installing VMware modules of ${EDITION,,}-${VERSION}"

    git clone https://github.com/mkubecek/vmware-host-modules.git -b "${EDITION,,}"-"${VERSION}"

    ## Fix KERNEL_VERSION neticf file
    if [[ $(uname -r | grep -Eo '^[^\d.].[^\d][^\d]') == 5.14 ]]; then
        sed -i 's/5, 15/5, 14/' vmnet-only/netif.c
    fi

    if ! make &>output.log; then
        echo "Please verify ${WORKDIR}/output.log"
        exit 10
    fi
    make install

    cd "${OLDPWD}" || echo "There is not OLDPWD"
    rm -rf "${WORKDIR}"
    set +x

}

function suspend_vm_before_host() {
    cat >/usr/lib/systemd/system-sleep/vmware.sleep <<\EOF
    set -eu

    if [[ $# -ne 2 ]]; then
        echo "Usage: $0 <period> <action>"
        exit 1
    fi

    period=$1
    action=$2

    echo "vmware system-sleep hook argv: ${period} ${action}"

    if ! command -v vmrun &>/dev/null; then
        echo "command not found: vmrun"
    fi

    if [[ "${period}" = "pre" ]]; then
        readarray -t vms < <(vmrun list | tail -n +2)

        echo "Number of running VMs: ${#vms[@]}"

        if [[ ${#vms[@]} -eq 0 ]]; then
            exit
        fi

        for vm in "${vms[@]}"; do
            echo -n "Suspending ${vm}... "
            vmrun suspend "${vm}"
            echo "done"
        done

        sleep 1
    else
        echo "Nothing to do"
    fi
EOF
}

######################MAIN

sudo_v
arguments_validate "$1"
detect_distro
install_dependencies
init.d-fix

## Bundle Installation
case $1 in
VMware-Remote*)
    bash "$1" --eulas-agreed
    ;;
VMware-Workstation* | VMware-Player*)
    module_compilation "$1"
    bash "$1" --eulas-agreed --deferred-gtk
    ;;
*)
    echo "Invalid file"
    ;;
esac

## If secure boot is enabled, signin the modules of vmware
if mokutil --sb-state | grep -qi enabled; then
    openssl req -new -x509 -newkey rsa:2048 -keyout vmware.priv -outform DER -out vmware.der -nodes -days 36500 -subj "/CN=VMware/" -addext "extendedKeyUsage=codeSigning"
    /lib/modules/"$(uname -r)"/build/scripts/sign-file sha256 ./vmware.priv ./vmware.der "$(/sbin/modinfo -n vmmon)"
    /lib/modules/"$(uname -r)"/build/scripts/sign-file sha256 ./vmware.priv ./vmware.der "$(/sbin/modinfo -n vmnet)"
    mokutil --root-pw --import vmware.der
fi

## fix VMWare LSB
if [ -f /etc/init.d/vmware ]; then
    ex -s /etc/init.d/vmware -c '/^# Basic support for IRIX style chkconfig/-2r /dev/stdin' -c 'wq' <<'EOF'
### BEGIN INIT INFO
# Required-Start:    $remote_fs $syslog
# Required-Stop:     $remote_fs $syslog
# Default-Start:     2 3 4 5
# Default-Stop:      0 1 6
### END INIT INFO
EOF
fi

file_permissions_vmnet_group
create_vmware.service
suspend_vm_before_host

if mokutil --sb-state | grep -qi enabled; then
    clear
    echo "Please reboot the system"
fi

echo "Error 8 : VMware Remote Console detected; unable to proceed with VMware"
